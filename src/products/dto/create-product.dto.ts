/* eslint-disable prettier/prettier */
export class CreateProductDto {
    name: string;
    price: string;
    image: string;
    type: string;
}
